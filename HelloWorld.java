package com.muhaiminabdu.javaproject;

import java.io.IOException;
import java.util.Scanner;

public class HelloWorld {
	
	public static String operations;
	private static Scanner num;
	public static void main(String[] args) {
		int w = 0;
		int z = 0;
		int res = 0;
		try {
			num = new Scanner(System.in);
			System.out.println("Enter a number:\n");
			w = num.nextInt();
			System.out.println("Enter a number:\n");
			z = num.nextInt();
			res = calculate(w,z);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(res);
	}
	
	public static int calculate(int x,int y) throws IOException {
		Scanner in = new Scanner(System.in);
		try {
			int result = 0;
			System.out.println("Enter the operation you wish to do:\n");
			operations = in.nextLine();		
			switch(operations) {
				case "+":
					result = x + y;
					break;
				case "-":
					result = x - y;
					break;
				case "*":
				case "x":
					result = x * y;
					break;
				case "/":
					result = x / y;
					break;
				default:
					System.out.println("The operation is invalid");
			}
			return result;
			
		}
		finally {
			in.close();
		}
	}
}
